export module voxanne.stx.assume;

import <source_location>;
import <stdexcept>;
import <format>;
import voxanne.config;

namespace voxanne::stx {

	export template<typename T>
	requires requires (T v) { !v; } // Rough boolean-testability
	void assume(T const& val, std::source_location loc = std::source_location::current()) {
		if constexpr (BuildType != Build::Release) {
			if (!val)
				throw std::runtime_error(std::format("Assumption failed at {}:{}", loc.file_name(), loc.line()));
		} else {
#ifdef _MSC_VER
			__assume(val);
#endif
		}
	}

}
