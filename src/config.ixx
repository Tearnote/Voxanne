export module voxanne.config;

import <string_view>;
import <array>;

namespace voxanne {

	export inline constexpr auto AppTitle = "Voxanne";
	export inline constexpr auto AppVersion = std::to_array({ 0u, 0u, 0u });
	
	// Expose build type
#define BUILD_DEBUG 0
#define BUILD_RELDEB 1
#define BUILD_RELEASE 2
#ifndef BUILD_TYPE
#error Build type not defined
#endif
	export enum class Build {
		Debug,
		RelDeb,
		Release,
	};
	export inline constexpr auto BuildType =
#if BUILD_TYPE == BUILD_DEBUG
		Build::Debug
#elif BUILD_TYPE == BUILD_RELDEB
		Build::RelDeb
#elif BUILD_TYPE == BUILD_RELEASE
		Build::Release
#else
#error Build type incorrectly defined
#endif
	;

	// Whether to name threads for debugging
	export inline constexpr auto DebugThreads = (BuildType != Build::Release);

	// Whether Vulkan validation layers are enabled
	export inline constexpr auto VulkanValidation = (BuildType == Build::Debug);

	// Logfile location
	export inline constexpr auto LogfilePath = []() -> std::string_view {
		switch (BuildType) {
			case Build::Debug:   return "voxanne-debug.log";
			case Build::RelDeb:  return "voxanne-reldeb.log";
			case Build::Release: return "voxanne.log";
		}
	}();

}
