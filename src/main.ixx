export module voxanne.main;

import <exception>;
import <chrono>;
import <thread>;
import <string>;
import <Windows.h>;
import voxanne.sys.os;
import voxanne.config;
import voxanne.log;

using namespace voxanne; // Can't namespace WinMain

auto WinMain(HINSTANCE, HINSTANCE, LPSTR, int) -> int try {
	using namespace std::chrono_literals;
	sys::setThreadName("main");
	sys::createConsole();
	sys::logToDebug(std::format("{}\n", LogfilePath));
	l = Logger({
		.consoleLevel = Logger::Level::Debug,
		.fileLevel = Logger::Level::Debug,
		.debugLevel = Logger::Level::Debug,
		.filename = std::string(LogfilePath),
	});
	l->info("Hello world!");
	auto period = sys::SchedulerPeriod(1ms);
	using namespace std::chrono_literals;
	std::this_thread::sleep_for(2s);
	return 0;
} catch (std::exception e) {
	l->error("Unhandled exception: {}", e.what());
	return 1;
}
