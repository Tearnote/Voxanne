export module voxanne.log;

import <string_view>;
import <optional>;
import <sstream>;
import <utility>;
import <iomanip>;
import <cstring>;
import <cstdio>;
import <format>;
import <string>;
import <cerrno>;
import <ctime>;
import <print>;
import <array>;
import voxanne.stx.assume;
import voxanne.sys.os;

// https://developercommunity.visualstudio.com/t/visual-studio-cant-find-time-function-using-module/1126857
#ifdef _MSC_VER
#define WIN32_CTIME_ISSUE
#endif

namespace voxanne {

	export class Logger {
	public:
		enum class Level {
			Debug,
			Info,
			Warn,
			Error,
		};

		struct Config {
			std::optional<Level> consoleLevel;
			std::optional<Level> fileLevel;
			std::optional<Level> debugLevel;
			std::optional<std::string> filename; // Only used if fileLevel exists
		};

		Logger(Config&& config):
			m_config(std::move(config))
		{
			// Open logfile if requested
			if (config.fileLevel) {
				stx::assume(m_config.filename);
				auto* file = std::fopen(m_config.filename->c_str(), "w");
				if (!file) throw std::runtime_error(std::format(
					"Couldn't open log file \"{}\" for writing: {}",
					*m_config.filename,
					std::strerror(errno)
				));
				m_logfile = FilePtr(file);
			}
		}

		// Moveable
		Logger(Logger&&) = default;
		auto operator=(Logger&&)->Logger & = default;

		template<typename... Args>
		void log(Level level, std::string_view fmt, Args&&... args) {
			auto level_v = std::to_underlying(level);
			auto msg = std::vformat(fmt, std::make_format_args(std::forward<Args>(args)...));

#ifdef WIN32_CTIME_ISSUE
			auto timestamp = _time64(nullptr);
			auto* tm = _localtime64(&timestamp);
#else
			auto timestamp = std::time(nullptr);
			auto* tm = std::localtime(&timestamp);
#endif
			auto timestampStr = std::stringstream();
			timestampStr << std::put_time(tm, "%H:%M:%S");

			if (level_v >= std::to_underlying(*m_config.debugLevel))
				sys::logToDebug(std::format("{} [{}] {}\n", timestampStr.view(), LevelStr[level_v], msg));
			if (level_v >= std::to_underlying(*m_config.consoleLevel)) {
				auto* confile = level_v >= std::to_underlying(Level::Warn) ? stderr : stdout;
				std::print(confile, "{}{} {}{}\n", Colors[level_v], timestampStr.view(), msg, Colors.back());
			}
			if (level_v >= std::to_underlying(*m_config.fileLevel)) {
				std::print(m_logfile.get(), "{} [{}] {}\n", timestampStr.view(), LevelStr[level_v], msg);

				// Flush after important messages to avoid losing data
				if (level_v >= std::to_underlying(Level::Warn))
					std::fflush(m_logfile.get());
			}
		}

		// Convenience methods

		template<typename... Args>
		void debug(std::string_view fmt, Args&&... args) {
			log(Logger::Level::Debug, fmt, std::forward<Args>(args)...);
		}

		template<typename... Args>
		void info(std::string_view fmt, Args&&... args) {
			log(Logger::Level::Info, fmt, std::forward<Args>(args)...);
		}

		template<typename... Args>
		void warn(std::string_view fmt, Args&&... args) {
			log(Logger::Level::Warn, fmt, std::forward<Args>(args)...);
		}

		template<typename... Args>
		void error(std::string_view fmt, Args&&... args) {
			log(Logger::Level::Error, fmt, std::forward<Args>(args)...);
		}

	private:
		constexpr static auto LevelStr = std::to_array({
			"DBG", // Debug
			"INF", // Info
			"WRN", // Warn
			"ERR", // Error
		});

		constexpr static auto Colors = std::to_array({
			"\x1B[32m", // Debug
			"\x1B[36m", // Info
			"\x1B[33m", // Warn
			"\x1B[31m", // Error
			"\033[0m",  // Clear
		});

		using FilePtr = std::unique_ptr<std::FILE, decltype([](auto* f) {
			std::fclose(f);
		})>;

		Config m_config;
		FilePtr m_logfile;
	};

	// Global logger instance
	export inline auto l = std::optional<Logger>();

}
