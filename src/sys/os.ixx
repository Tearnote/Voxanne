module;

#ifndef NOMINMAX
#define NOMINMAX
#endif //NOMINMAX
#ifdef WIN32_LEAN_AND_MEAN
#undef WIN32_LEAN_AND_MEAN // Required for timeapi
#endif //WIN32_LEAN_AND_MEAN
#include <Windows.h>

export module voxanne.sys.os;

import <string_view>;
import <stdexcept>;
import <utility>;
import <codecvt>;
import <chrono>;
import <string>;
import <cstdio>;
import <fcntl.h>;
import <io.h>;
import voxanne.stx.assume;
import voxanne.config;

namespace voxanne::sys {

	using namespace std::chrono_literals;

	export class SchedulerPeriod {
	public:
		explicit SchedulerPeriod(std::chrono::milliseconds period):
			period(period)
		{
			stx::assume(period != 0ms);
			if (timeBeginPeriod(period.count()) != TIMERR_NOERROR)
				throw std::runtime_error("Failed to initialize Windows timer");
		}

		~SchedulerPeriod() {
			if (period == 0ms) return;
			timeEndPeriod(1);
		}

		SchedulerPeriod(SchedulerPeriod&& other) {
			*this = std::move(other);
		}

		auto operator=(SchedulerPeriod&& other) -> SchedulerPeriod& {
			period = other.period;
			other.period = 0ms;
		}

	private:
		std::chrono::milliseconds period;
	};

	export void setThreadName(std::string_view name) {
		if constexpr (DebugThreads) {
			auto lname = std::wstring(name.begin(), name.end());
			SetThreadDescription(GetCurrentThread(), lname.c_str());
		}
	}

	// https://github.com/ocaml/ocaml/issues/9252#issuecomment-576383814
	export void createConsole() {
		AllocConsole();

		freopen("CONOUT$", "w", stdout);
		freopen("CONOUT$", "w", stderr);

		int fdOut = _open_osfhandle(reinterpret_cast<intptr_t>(GetStdHandle(STD_OUTPUT_HANDLE)), _O_WRONLY | _O_BINARY);
		int fdErr = _open_osfhandle(reinterpret_cast<intptr_t>(GetStdHandle(STD_ERROR_HANDLE)), _O_WRONLY | _O_BINARY);

		if (fdOut) {
			_dup2(fdOut, 1);
			_close(fdOut);
			SetStdHandle(STD_OUTPUT_HANDLE, reinterpret_cast<HANDLE>(_get_osfhandle(1)));
		}
		if (fdErr) {
			_dup2(fdErr, 2);
			_close(fdErr);
			SetStdHandle(STD_ERROR_HANDLE, reinterpret_cast<HANDLE>(_get_osfhandle(2)));
		}

		_dup2(_fileno(fdopen(1, "wb")), _fileno(stdout));
		_dup2(_fileno(fdopen(2, "wb")), _fileno(stderr));

		std::setvbuf(stdout, nullptr, _IONBF, 0);
		std::setvbuf(stderr, nullptr, _IONBF, 0);

		// Set console encoding to UTF-8
		SetConsoleOutputCP(65001);

		// Enable ANSI color code support
		auto out = GetStdHandle(STD_OUTPUT_HANDLE);
		auto mode = 0ul;
		GetConsoleMode(out, &mode);
		mode |= ENABLE_VIRTUAL_TERMINAL_PROCESSING;
		SetConsoleMode(out, mode);
	}

	export void logToDebug(std::string_view msg) {
#ifdef _MSC_VER
		if constexpr (BuildType != Build::Release)
			OutputDebugStringW(std::wstring_convert<std::codecvt_utf8<wchar_t>>().from_bytes(msg.data(), msg.data() + msg.size()).c_str());
#endif
	}

}
